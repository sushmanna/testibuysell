NOW=$(date +"%Y-%m-%d-%r")
NEWBRANCH=$(date +"%Y-%m-%d-%I-%M-%S")
echo "Please enter some input for commit: "
read input_variable
echo "You entered for commit: $input_variable"
git co -b $NEWBRANCH
git add -A
git commit -m  "$NOW : $input_variable"
git co master
git pull origin master
git co $NEWBRANCH
git rebase master
git co master
git merge $NEWBRANCH
echo "all file commited"
