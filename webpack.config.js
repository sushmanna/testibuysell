var webpack = require('webpack');
var ReloadPlugin = require('webpack-reload-plugin');
var path = require('path');
var fs = require('fs');
var cwd = process.cwd();
var version = require(path.resolve(cwd, 'package.json')).version;
var config = {
    context: path.resolve(cwd, "src"),
    entry: findEntries(['webPackjs.js', 'webPackstyle.js']),
    output: {
        path: path.resolve(cwd, "www"),
        filename: "[name]/bundle.js",
        publicPath: '../'
    },
    module: {
        loaders: [
            {test: /\.css$/, exclude: /bootstrap\.css$/, loader: "style-loader!css-loader"},
            {test: /\.less$/, loader: "style-loader!css-loader!less-loader"},
            {test: /\.(png|jpg|gif)$/, loader: "file-loader?limit=50000&name=[path][name].[ext]"},
            {test: /\.eot?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader?name=[path][name].[ext]"},
            {test: /\.ttf?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader?name=[path][name].[ext]"},
            {test: /\.woff?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader?name=[path][name].[ext]"},
            {test: /\.woff2?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader?name=[path][name].[ext]"},
            {test: /\.svg?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader?name=[path][name].[ext]"},
            {test: /\.html$/, loader: "file-loader?name=[path][name].[ext]"},
            {test: /\.woff$/, loader: "file-loader?name=[path][name].[ext]"},
            {test: /\.woff2$/, loader: "file-loader?name=[path][name].[ext]"}
        ]
    },
    /*plugins:[
    new webpack.DefinePlugin({
      VERSION: JSON.stringify(version),
    }),
    new ReloadPlugin(false)
  ]*/
};
/*config.plugins.push(new webpack.optimize.UglifyJsPlugin({mangle:false}));*/
function addEntries(fileName, entries) {

    // mains = [ 'xxxxxx/src/rpm/main.js', .... ]
    var mains = require('glob').sync(path.resolve(cwd, 'src', '**', fileName));
    //console.log(mains);

    mains.forEach(function(file) {
        var entry = "./" + file.substr(5 + cwd.length, file.length - 8 - cwd.length);
        var name = entry.substr(9, entry.length - 1) || './';
        entries[name] = entry;
    });
}
function findEntries(fileNames) {
    var entries = {};
    fileNames.forEach(function(fileName) {
        addEntries(fileName, entries);
    });
     console.log(entries);
    return entries;
}
/**
 * Enable minification
 */



module.exports = config;