(function() {
    angular.module('ibuysell').controller('uploadProductController', uploadProductController);
    uploadProductController.$inject = ['$scope', '$rootScope', '$ionicModal', '$timeout', '$firebaseObject', '$firebaseArray', '$firebaseAuth'];
    function uploadProductController($scope, $rootScope, $ionicModal, $timeout, $firebaseObject, $firebaseArray, $firebaseAuth) {
        var ib = this;
        var ref = new Firebase("https://ibuysell.firebaseio.com/");
        var ref1 = new Firebase("https://ibuysell.firebaseio.com/product");

        ib.groups = [];
        for (var i = 0; i < 2; i++) {
            ib.groups[i] = {
                name: i,
                items: []
            };
            for (var j = 0; j < 3; j++) {
                ib.groups[i].items.push(i + '-' + j);
            }
        }

        /*
         * if given group is the selected group, deselect it
         * else, select the given group
         */
        ib.toggleGroup = function(group) {
            if (ib.isGroupShown(group)) {
                ib.shownGroup = null;
            } else {
                ib.shownGroup = group;
            }
        };
        ib.isGroupShown = function(group) {
            return ib.shownGroup === group;
        };

    }
})();