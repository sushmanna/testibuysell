(function() {
    angular.module('ibuysell').controller('RegistrationController', RegistrationController);
    RegistrationController.$inject = ['$scope', '$rootScope', '$ionicModal', '$timeout', '$ionicLoading', 'UserService', 'ionicToast', '$location'];
    function RegistrationController($scope, $rootScope, $ionicModal, $timeout, $ionicLoading, UserService, ionicToast, $location) {
        var ib = this;
        //normal register
        ib.register = function() {
            $ionicLoading.show({template: '<ion-spinner icon="bubbles"></ion-spinner>'});
            UserService.register(ib.logindata).success(function(response, status)
            {
                $ionicLoading.hide();
                if (status == 200) {
                    ionicToast.show(response.msg, 'top', true, 2500);
                    $location.path("/app/login");
                }
                else
                {
                    ionicToast.show(response.msg, 'top', true, 2500);
                }
            });
        };
    }
})();
