(function() {
    angular.module('ibuysell').controller('MyAccountSettingsController', MyAccountSettingsController);
    MyAccountSettingsController.$inject = ['$scope', '$rootScope', '$ionicModal', '$timeout', '$localStorage', 'UserService', '$ionicLoading', 'ionicToast'];
    function MyAccountSettingsController($scope, $rootScope, $ionicModal, $timeout, $localStorage, UserService, $ionicLoading, ionicToast) {
        var ib = this;
        ib.name = 'ame';
        ib.init = function()
        {
            ib.userdata = $localStorage.loginData;
        }
        ib.updateProfile = function()
        {
            $ionicLoading.show({template: '<ion-spinner icon="bubbles"></ion-spinner>'});
            UserService.updateProfile(ib.userdata).success(function(response, status)
            {
                $ionicLoading.hide();
                if (status == 200) {
                    ionicToast.show(response.msg, 'top', true, 2500);
                } else
                {
                    ionicToast.show(response.msg, 'top', true, 2500);
                }
            });
        }
    }
})();
