(function() {
    angular.module('ibuysell').controller('LoginController', LoginController);
    LoginController.$inject = ['$scope', '$rootScope', '$ionicModal', '$timeout', '$location', 'UserService', 'ionicToast', '$ionicLoading', '$firebaseAuth', '$ionicModal', '$localStorage'];
    function LoginController($scope, $rootScope, $ionicModal, $timeout, $location, UserService, ionicToast, $ionicLoading, $firebaseAuth, $ionicModal, $localStorage) {
        var ib = this;
        ib.logindata = {};
        var ref = new Firebase("https://ibuysell.firebaseio.com/");
        $ionicModal.fromTemplateUrl('js/module/user/templates/emailModal.html', {
            scope: $scope
        }).then(function(modal) {
            ib.loginModal = modal;
        });
        //normal login
        ib.login = function() {
            $ionicLoading.show({template: '<ion-spinner icon="bubbles"></ion-spinner>'});
            UserService.login(ib.logindata).success(function(response, status)
            {
                $ionicLoading.hide();
                if (status == 200) {
                    $localStorage.loginData = response.data;
                    $location.path("/app/myaccount");
                } else
                {
                    ionicToast.show(response.msg, 'top', true, 2500);
                }
            });
        };
        //auth login
        ib.authLogin = function(loginType) {
            $ionicLoading.show({template: '<ion-spinner icon="bubbles"></ion-spinner>'});
            var auth = $firebaseAuth(ref);
            auth.$authWithOAuthPopup(loginType)
                    .then(function(authData) {
                        $ionicLoading.hide();
                        ib.logindata.provider = loginType;
                        if (loginType == 'facebook') {
                            ib.logindata.fname = authData.facebook.displayName;
                            ib.logindata.authId = authData.facebook.id;
                        }
                        else if (loginType == 'twitter')
                        {
                            ib.logindata.fname = authData.twitter.displayName;
                            ib.logindata.authId = authData.twitter.id;
                        }


                        UserService.checkUser(ib.logindata).success(function(response, status)
                        {
                            if (status == 200) {
                                $localStorage.loginData = response.data;
                                $location.path("/app/myaccount");
                            } else
                            {
                                ib.action = 'login';
                                ib.errormsg = '';
                                ib.loginModal.show();
                            }
                        });
                    }, function(error) {
                        ionicToast.show(error, 'top', true, 2500);
                        $ionicLoading.hide();
                    })
                    .catch(function(error) {
                        ionicToast.show("Authentication failed:".error, 'top', true, 2500);
                    });
        };

        //close email modal
        ib.closeLogin = function() {
            ib.loginModal.hide();
        };
        //open modal for forgot password
        ib.openModal = function() {
            ib.action = 'forgot';
            ib.errormsg = '';
            ib.loginModal.show();
        };
        //register and login
        ib.doLogin = function(action) {
            if (action == 'login')
            {
                UserService.authRegister(ib.logindata).success(function(response, status)
                {
                    if (status == 200) {
                        $localStorage.loginData = response.data;
                        ib.loginModal.hide();
                        $location.path("/app/myaccount");
                    } else
                    {
                        ib.errormsg = response.msg;
                    }
                });
            } else if (action == 'forgot')
            {
                UserService.forgotpassword(ib.logindata).success(function(response, status)
                {
                    console.log(status);
                    if (status == 200) {
                        ionicToast.show(response.msg, 'top', true, 2500);
                        ib.loginModal.hide();
                        $location.path("/app/login");
                    } else
                    {
                        ib.errormsg = response.msg;
                    }
                });
            }
        };
    }
})();
