angular.module('ibuysell', ['ionic', 'firebase', 'ionic-toast', 'ngStorage'])
        .run(function ($ionicPlatform, $localStorage, $location, $rootScope) {
            $ionicPlatform.ready(function () {
                if (window.cordova && window.cordova.plugins.Keyboard) {
                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                    cordova.plugins.Keyboard.disableScroll(true);
                }
                if (window.StatusBar) {
                    StatusBar.styleDefault();
                }
                if ($localStorage.loginData)
                {
                    $location.path("/app/myaccount");
                }
            });
            $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
                if ($localStorage.loginData)
                {
                    $location.path("/app/myaccount");
                }
            });
        })
        .config(function ($stateProvider, $urlRouterProvider) {
            $stateProvider
                    .state('app', {
                        url: '/app',
                        abstract: true,
                        templateUrl: 'js/module/product/templates/categorysidebar.html',
                        controller: 'productSidebarController'
                    })
                    .state('app.home', {
                        url: '/home',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/home/templates/home.html',
                                controller: 'HomeController as hm'
                            }
                        }
                    })
                    .state('app.productDetails', {
                        url: '/productdetails',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/product/templates/productdetails.html',
                                controller: 'productDetailscontroller as pd'
                            }
                        }
                    })
                    .state('app.uploadproduct', {
                        url: '/uploadproduct',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/product/templates/uploadProduct.html',
                                controller: 'uploadProductController as up'
                            }
                        }
                    })
                    .state('app.search', {
                        url: '/search',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/product/templates/search.html',
                                controller: 'searchController as sc'
                            }
                        }
                    })
                    .state('app.taglist', {
                        url: '/taglist',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/product/templates/taglist.html',
                                controller: 'tagListController as tl'
                            }
                        }
                    })
                    .state('app.cart', {
                        url: '/cart',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/product/templates/cart.html',
                                controller: 'cartController as cart'
                            }
                        }
                    })
                    .state('app.payment', {
                        url: '/payment',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/product/templates/payment.html',
                                controller: 'paymentController as pay'
                            }
                        }
                    })
                    .state('app.login', {
                        url: '/login',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/user/templates/login.html',
                                controller: 'LoginController as lc'
                            }
                        }
                    })
                    .state('app.myaccount', {
                        url: '/myaccount',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/user/templates/myaccount.html',
                                controller: 'MyAccountController as ma'
                            }
                        }
                    })
                    .state('app.mysetting', {
                        url: '/mysetting',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/user/templates/myaccountsetting.html',
                                controller: 'MyAccountSettingsController as ms'
                            }
                        }
                    })
                    .state('app.shareapp', {
                        url: '/shareapp',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/user/templates/shareapp.html',
                                controller: 'ShareAppController as sa'
                            }
                        }
                    })
                    .state('app.registration', {
                        url: '/registration',
                        views: {
                            'menuContent': {
                                templateUrl: 'js/module/user/templates/registration.html',
                                controller: 'RegistrationController as rc'
                            }
                        }
                    });
            // if none of the above states are matched, use this as the fallback
            $urlRouterProvider.otherwise('/app/home');
        })
        .factory('serverUrls', serverUrls);
serverUrls.$inject = [];
function serverUrls() {
    var HOSTURL = 'http://98.191.125.87/dtswork/ibuysell';
    var urls = {
        login: HOSTURL + '/Login/api_userloginprocess',
        getuserbyauthid: HOSTURL + '/Login/api_authuserlogin',
        insertuserbyauthid: HOSTURL + '/Login/api_authuser_registration',
        register: HOSTURL + '/Login/api_newuserregistration',
        updateprofile: HOSTURL + '/Login/api_newuserregistration',
        forgot: HOSTURL + '/Login/api_userforgot_password'
    };
    return urls;
}
