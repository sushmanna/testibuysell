(function() {
    angular.module('ibuysell').factory('UserService', UserService);
    UserService.$inject = ['$http', 'serverUrls', '$window', '$location'];
    function UserService($http, serverUrls, $window, $location) {
        var data = {};
        data.register = function(userData) {
            return $http({
                url: serverUrls.register,
                method: 'POST',
                data: userData,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.authRegister = function(userData) {
            return $http({
                url: serverUrls.insertuserbyauthid,
                method: 'POST',
                data: userData,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.checkUser = function(userData) {
            return $http({
                url: serverUrls.getuserbyauthid,
                method: 'POST',
                data: userData,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.login = function(userData) {
            return $http({
                url: serverUrls.login,
                method: 'POST',
                data: userData,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.forgotpassword = function(userData) {
            return $http({
                url: serverUrls.forgot,
                method: 'POST',
                data: userData,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        data.updateProfile = function(userData) {
            return $http({
                url: serverUrls.updateprofile,
                method: 'POST',
                data: userData,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                responseText: 'JSON'
            });
        };
        return data;
    }
})();
