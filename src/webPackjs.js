define(function (require, exports, module) {
    //lib
    require('file?name=lib/ionic/js/ionic.bundle.js!./lib/ionic/js/ionic.bundle.js');
    require('file?name=lib/firebase/firebase.js!./lib/firebase/firebase.js');
    require('file?name=lib/angularfire/dist/angularfire.min.js!./lib/angularfire/dist/angularfire.min.js');
    require('file?name=lib/ionic-toast/dist/ionic-toast.bundle.min.js!./lib/ionic-toast/dist/ionic-toast.bundle.min.js');
    require('file?name=lib/ngstorage/ngStorage.min.js!./lib/ngstorage/ngStorage.min.js');
    
    //img
    require('./img/home.png'); 
    require('./img/top-logo.png'); 
    require('./img/user-img.png'); 
    require('./img/blue-album.jpg'); 
    
    
    //script
    require('./js/app.js'); 
    require('./js/Service/UserService.js');
    require('./js/module/home/controllers/HomeController.js');
    require('./js/module/product/controllers/productSidebarController.js');
    require('./js/module/product/controllers/uploadProductController.js'); 
    require('./js/module/product/controllers/searchController.js');
    require('./js/module/product/controllers/tagListController.js');
    require('./js/module/product/controllers/cartController.js');
    require('./js/module/product/controllers/paymentController.js');
    require('./js/module/user/controllers/LoginController.js');
    require('./js/module/user/controllers/RegistrationController.js');
    require('./js/module/user/controllers/MyAccountController.js');
    require('./js/module/user/controllers/MyAccountSettingsController.js');
    require('./js/module/user/controllers/ShareAppController.js');
    require('./js/Directives/accountTopDirective.js');
    require('./js/Directives/accountFooterDirective.js');
    
    
    //html screen
    require('./index.html');
    require('./js/module/product/templates/categorysidebar.html');
    require('./js/module/home/templates/home.html');
    require('./js/module/product/templates/productdetails.html');
    require('./js/module/product/templates/uploadProduct.html'); 
    require('./js/module/product/templates/search.html');
    require('./js/module/product/templates/taglist.html');
    require('./js/module/product/templates/payment.html');
    require('./js/module/product/templates/cart.html');
    require('./js/module/user/templates/login.html');
    require('./js/module/user/templates/myaccount.html');
    require('./js/module/user/templates/myaccountsetting.html');
    require('./js/module/user/templates/registration.html');
    require('./js/module/user/templates/emailModal.html');
    require('./js/module/user/templates/shareapp.html');
    require('./js/Directives/templates/accounttop.html');
    require('./js/Directives/templates/accountfooter.html');
})